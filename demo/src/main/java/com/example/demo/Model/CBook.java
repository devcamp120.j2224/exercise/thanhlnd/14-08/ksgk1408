package com.example.demo.Model;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;

@Entity
@Table(name = "book_table")
public class CBook {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @Column(name = "ma_chuong",unique = true)
    @NotEmpty(message = "không được để trống mã chương")
    private String maChuong;

    @Column(name = "ten_chuong")
    @NotEmpty(message = "không được để trống tên chương")
    private String tenChuong;

    @Column(name = "gioi_thieu")
    private String gioiThieu;

    @Column(name = "ten_nguoi_dich")
    private String tenNguoiDich;

    @Column(name = "trang")
    private String trang;

    @OneToMany(targetEntity = CChapter.class, cascade = CascadeType.ALL)
    @JoinColumn(name = "book_id")
    private Set<CChapter> chapters;
    
    public CBook() {
    }

    public CBook(long id, String maChuong, String tenChuong, String gioiThieu, String tenNguoiDich, String trang,
            Set<CChapter> chapters) {
        this.id = id;
        this.maChuong = maChuong;
        this.tenChuong = tenChuong;
        this.gioiThieu = gioiThieu;
        this.tenNguoiDich = tenNguoiDich;
        this.trang = trang;
        this.chapters = chapters;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getMaChuong() {
        return maChuong;
    }

    public void setMaChuong(String maChuong) {
        this.maChuong = maChuong;
    }

    public String getTenChuong() {
        return tenChuong;
    }

    public void setTenChuong(String tenChuong) {
        this.tenChuong = tenChuong;
    }

    public String getGioiThieu() {
        return gioiThieu;
    }

    public void setGioiThieu(String gioiThieu) {
        this.gioiThieu = gioiThieu;
    }

    public String getTenNguoiDich() {
        return tenNguoiDich;
    }

    public void setTenNguoiDich(String tenNguoiDich) {
        this.tenNguoiDich = tenNguoiDich;
    }

    public String getTrang() {
        return trang;
    }

    public void setTrang(String trang) {
        this.trang = trang;
    }

    public Set<CChapter> getChapters() {
        return chapters;
    }

    public void setChapters(Set<CChapter> chapters) {
        this.chapters = chapters;
    }

    

    
}
    