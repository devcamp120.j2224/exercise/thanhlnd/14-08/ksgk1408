package com.example.demo.Model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "chapter_table")
public class CChapter {
    
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @Column(name = "ma_bai",unique = true)
    @NotEmpty(message = "không được để trống mã bài")
    private String maBai;

    @Column(name = "ten_bai")
    @NotEmpty(message = "không được để trống tên bài")
    private String tenBai;

    @Column(name = "gioi_thieu")
    private String gioiThieu;

    @Column(name = "trang")
    private String trang;

    @ManyToOne
    @JsonIgnore
    private CBook book;
    
    public CChapter() {
    }

    public CChapter(long id, String maBai, String tenBai, String gioiThieu, String trang, CBook book) {
        this.id = id;
        this.maBai = maBai;
        this.tenBai = tenBai;
        this.gioiThieu = gioiThieu;
        this.trang = trang;
        this.book = book;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getMaBai() {
        return maBai;
    }

    public void setMaBai(String maBai) {
        this.maBai = maBai;
    }

    public String getTenBai() {
        return tenBai;
    }

    public void setTenBai(String tenBai) {
        this.tenBai = tenBai;
    }

    public String getGioiThieu() {
        return gioiThieu;
    }

    public void setGioiThieu(String gioiThieu) {
        this.gioiThieu = gioiThieu;
    }

    public String getTrang() {
        return trang;
    }

    public void setTrang(String trang) {
        this.trang = trang;
    }

    public CBook getBook() {
        return book;
    }

    public void setBook(CBook book) {
        this.book = book;
    }

   
    
    
}
