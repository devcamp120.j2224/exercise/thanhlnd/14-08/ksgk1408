package com.example.demo.Repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.demo.Model.CChapter;

public interface IChapterRepository extends JpaRepository<CChapter,Long>{
    
}
