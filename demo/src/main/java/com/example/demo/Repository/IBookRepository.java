package com.example.demo.Repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.demo.Model.CBook;

public interface IBookRepository extends JpaRepository<CBook,Long>{
    
}
