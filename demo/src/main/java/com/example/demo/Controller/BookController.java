package com.example.demo.Controller;

import java.util.*;

import javax.validation.Valid;

import org.aspectj.apache.bcel.classfile.Module.Require;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.Model.CBook;
import com.example.demo.Repository.IBookRepository;

@RestController
@CrossOrigin
@RequestMapping("/book")
public class BookController {

    @Autowired
    IBookRepository iBookRepository;

    // Tìm tất cả book
    @GetMapping("/all")
    public ResponseEntity<List<CBook>> getAllBook() {
        try {
            List<CBook> listBooks = new ArrayList();
            iBookRepository.findAll().forEach(listBooks::add);
            if (listBooks.size() == 0) {
                return new ResponseEntity<List<CBook>>(listBooks, HttpStatus.NOT_FOUND);
            } else {
                return new ResponseEntity<List<CBook>>(listBooks, HttpStatus.OK);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // tìm book theo id
    @GetMapping("/detail/{id}")
    public ResponseEntity<Object> getBookById(@PathVariable("id") long id) {
        Optional<CBook> bookFounded = iBookRepository.findById(id);
        if (bookFounded.isPresent()) {
            return new ResponseEntity<Object>(bookFounded, HttpStatus.OK);
        } else {
            return new ResponseEntity<Object>(null, HttpStatus.NOT_FOUND);
        }
    }

    // tạo mới book
    @PostMapping("/create")
    public ResponseEntity<Object> createBook(@Valid @RequestBody CBook book) {
        Optional<CBook> creatBookData = iBookRepository.findById(book.getId());
        try {
            if (creatBookData.isPresent()) {
                return ResponseEntity.unprocessableEntity().body("Book already exsit");
            }
            book.setMaChuong(book.getMaChuong());
            book.setTenChuong(book.getTenChuong());
            book.setTenNguoiDich(book.getTenNguoiDich());
            book.setGioiThieu(book.getGioiThieu());
            book.setTrang(book.getTrang());

            CBook saveBook = iBookRepository.save(book);
            return new ResponseEntity<>(saveBook, HttpStatus.CREATED);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // update book bằng id
    @PutMapping("/update/{id}")
    public ResponseEntity<Object> updateBookById( @PathVariable("id") long id,@Valid @RequestBody CBook book) {
        Optional<CBook> updateBookData = iBookRepository.findById(id);
        if (updateBookData.isPresent()) {
            CBook saveBook = updateBookData.get();

            saveBook.setMaChuong(book.getMaChuong());
            saveBook.setTenChuong(book.getTenChuong());
            saveBook.setTenNguoiDich(book.getTenNguoiDich());
            saveBook.setGioiThieu(book.getGioiThieu());
            saveBook.setTrang(book.getTrang());

            try {
                return ResponseEntity.ok(iBookRepository.save(saveBook));
            } catch (Exception e) {
                // TODO: handle exception
                return ResponseEntity.unprocessableEntity()
                        .body("Can not execute operation of this Entity" + e.getCause().getCause().getMessage());
            }
        } else {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // delete book bằng id
    @DeleteMapping("/delete/{id}")
    public ResponseEntity<CBook> deleteBookById(@PathVariable("id") long id) {
        try {
            iBookRepository.deleteById(id);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
