package com.example.demo.Controller;

import java.util.*;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.Model.CBook;
import com.example.demo.Model.CChapter;
import com.example.demo.Repository.IBookRepository;
import com.example.demo.Repository.IChapterRepository;

@RestController
@CrossOrigin
@RequestMapping("/chapter")
public class ChapterController {
    @Autowired
    IChapterRepository iChapterRepository;
    @Autowired
    IBookRepository iBookRepository;

    // tìm tất cả chapter
    @GetMapping("/all")
    public List<CChapter> getAllChapter() {
        return iChapterRepository.findAll();
    }

    // tìm chapter bằng id
    @GetMapping("/detail/{id}")
    public CChapter getChapterById(@PathVariable Long id) {
        if (iChapterRepository.findById(id).isPresent())
            return iChapterRepository.findById(id).get();
        else
            return null;
    }

    // tạo thêm mới chapter
    @PostMapping("/create/{id}")
    public ResponseEntity<Object> createChapter( @PathVariable("id") Long id,@Valid @RequestBody CChapter chapter) {
        Optional<CBook> bookData = iBookRepository.findById(id);

        Optional<CChapter> chapterFound = iChapterRepository.findById(id);

        if (bookData.isPresent()) {
            try {
                if (chapterFound.isPresent()) {
                    return ResponseEntity.unprocessableEntity().body(" Chapter already exsit ");
                }

                CBook booknewdata = bookData.get();
                chapter.setBook(booknewdata);

                chapter.setMaBai(chapter.getMaBai());
                chapter.setTenBai(chapter.getTenBai());
                chapter.setGioiThieu(chapter.getGioiThieu());
                chapter.setTrang(chapter.getTrang());
                CChapter chapternewdata = iChapterRepository.save(chapter);

                return new ResponseEntity<Object>(chapternewdata, HttpStatus.CREATED);
            } catch (Exception e) {
                return ResponseEntity.unprocessableEntity()
                        .body("Can not execute operation about this Entity " + e.getCause().getMessage());
            }
        } else {
            return ResponseEntity.badRequest().body("Failed to get specified id: " + id);
        }
    }

    // update chapter bằng id
    @PutMapping("/update/{id}")
    public ResponseEntity<Object> updateChapter( @PathVariable("id") long id,@Valid @RequestBody CChapter chapter) {
        Optional<CChapter> chapterData = iChapterRepository.findById(id);
        if (chapterData.isPresent()) {
            CChapter newChapter = chapterData.get();
            newChapter.setMaBai(chapter.getMaBai());
            newChapter.setTenBai(chapter.getTenBai());
            newChapter.setGioiThieu(chapter.getGioiThieu());
            newChapter.setTrang(chapter.getTrang());

            try {
                return ResponseEntity.ok(iChapterRepository.save(newChapter));
            } catch (Exception e) {
                // TODO: handle exception
                return ResponseEntity.unprocessableEntity()
                        .body("Can not execute operation of this Entity" + e.getCause().getCause().getMessage());
            }
        } else {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // xóa chapter by id
    @DeleteMapping("/delete/{id}")
    public ResponseEntity<Object> deleteChapterById(@PathVariable Long id) {
        try {
            iChapterRepository.deleteById(id);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
